///////////////////
///CSE 2 hw01
//
public class WelcomeClass {
  
  public static void main(String[]agrs){
    ///Prints Welcome message
    System.out.println("  ----------- ");
    System.out.println("  | Welcome | ");
    System.out.println("  ----------- ");
    /////Prints ID number
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-T--J--B--2--2--0->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v");
    /////Autobiography 
    System.out.println(" Hello, my name is Travis Barnes \n I'm a sophomore chemical engineer. \n I was born in Brookyln, New York but I was raised here in Bethlehem.");           
  }
}