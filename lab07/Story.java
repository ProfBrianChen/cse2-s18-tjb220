//////Travis Barnes
//////Lab 07
//////Story  
//////uses random number generators to create sentences
import java.util.Random;
import java.util.Scanner;
public class Story{
 ///Word generating methods 
  public static String Adjectives(int number){
    Random randomGenerator = new Random();
    number = randomGenerator.nextInt(9);
    String word = "";
    switch(number){
        case(0):
          word = "smart";
          break;
        case(1):
          word = "beautiful";
          break;
        case(2):
          word = "angry";
          break;
        case(3):
          word = "hyper";
          break;
        case(4):
          word = "loud";
          break;
        case(5):
          word = "awesome";
          break;
        case(6):
          word = "brave";
          break;
        case(7):
          word = "corny";
          break;
        case(8):
          word = "fake";
          break;        
    }
        return word;
  }
  public static String Verbs(int number){
    Random randomGenerator = new Random();
    number = randomGenerator.nextInt(9);
    String word = "";
    switch(number){
        case(0):
          word = "ate";
          break;
        case(1):
          word = "blasted";
          break;
        case(2):
          word = "fought";
          break;
        case(3):
          word = "rode";
          break;
        case(4):
          word = "cooked";
          break;
        case(5):
          word = "chopped";
          break;
        case(6):
          word = "broke";
          break;
        case(7):
          word = "built";
          break;
        case(8):
          word = "saved";
          break;        
    }
        return word;
  }
  public static String Subjects(int number){
    Random randomGenerator = new Random();
    number = randomGenerator.nextInt(9);
    String word = "";
    switch(number){
        case(0):
          word = "box";
          break;
        case(1):
          word = "monster";
          break;
        case(2):
          word = "corgi";
          break;
        case(3):
          word = "horse";
          break;
        case(4):
          word = "snail";
          break;
        case(5):
          word = "water bottle";
          break;
        case(6):
          word = "chair";
          break;
        case(7):
          word = "chef";
          break;
        case(8):
          word = "toad";
          break;        
    }
        return word;
  }  
  public static String Objects(int number){
    Random randomGenerator = new Random();
    number = randomGenerator.nextInt(9);
    String word = "";
    switch(number){
        case(0):
          word = "apple";
          break;
        case(1):
          word = "moon";
          break;
        case(2):
          word = "spaceship";
          break;
        case(3):
          word = "beach house";
          break;
        case(4):
          word = "boat";
          break;
        case(5):
          word = "table";
          break;
        case(6):
          word = "university";
          break;
        case(7):
          word = "computer";
          break;
        case(8):
          word = "tape";
          break;        
    }
        return word;
  }
  public static String firstWord(String subject){
    Random randomGenerator = new Random();
    int number = randomGenerator.nextInt(3);
    String word = "";
    switch(number){
        case(0):
          word = "The " + subject;
          break;
        case(1):
          word = "It";
          break;
        case(2):
          word = "This " + subject;
          break;       
    }
        return word;
  }
  
  ///Sentence methods
  public static String Thesis(int number){
    number = 0;
    String adj = Adjectives(number);
    String adj2 = Adjectives(number);      
    String verb = Verbs(number);
    String subject = Subjects(number); 
    String object = Objects(number);  
    System.out.println("The " + adj + " " + subject + " " + verb + " the " + adj2 + " " + object + ".");
    return subject;
  }
  public static String Action(String mainSubject){
    int number = 0;
    String first = firstWord(mainSubject);
    String adj = Adjectives(number);      
    String verb = Verbs(number);
    String subject = Subjects(number); 
    String object = Objects(number);  
    System.out.println(first + " found the " + object + " and " + verb + " the " + adj + " " + subject + ".");
    return subject; 
  }
  
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    int number = 0;
    int answer = 0;
    boolean story = false;
    boolean start = true;
    boolean restart = true;
    boolean answerNumber = false;
    boolean answerNumber2 = false;
    while(start == true){
    System.out.print("Do you want to make some sentences? (1 for yes/2 for no) ");  
    answerNumber = scan.hasNextInt();
      if(answerNumber == true){ 
        answer = scan.nextInt(); 
        if(answer == 1){
          start = false;
          story = true;
        }
        else if(answer == 2){
          System.out.println("See ya later");
          story = false;
          start = false;
        }
        else{
          start = true;
          System.out.print("Wrong number, ");
        }
      }
      else if(answerNumber == false){
        String junk = scan.next();
        System.out.print("Wrong input, ");
      }
    }  
    while(story == true){
      restart = true;
      String mainSubject = Thesis(number);  
      String secondSubject = Action(mainSubject);
      System.out.println("The " + mainSubject + " loves their " + secondSubject + ".");  
      while(restart == true){
        System.out.print("New story? (1/2)");
        answerNumber2 = scan.hasNextInt();
        if(answerNumber2 == true){
          answer = scan.nextInt();
          if(answer == 2){
            story = false;
            System.out.println("See ya later");
            break;
          } 
          else if(answer == 1){
            story = true;
            restart = false;
          }
          else{
          System.out.print("Wrong number, ");
          }
        }
        else if(answerNumber == false){
          String junk = scan.next();
          System.out.print("Wrong input, ");
        }
      } 
    }  
  }
}