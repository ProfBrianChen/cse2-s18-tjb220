///Travis Barnes 
///CSE002 HW8 Program 2
///Remove Elment
import java.util.Random;
import java.util.Scanner;
public class RemoveElements{
  public static int[] randomInput(){
    Random randomNumber = new Random();
    int[] num = new int[10];//sets an array size 10
    for(int i = 0; i < 10; i++){///moves from 0-9
      int number = randomNumber.nextInt(10);//picks 10 random numbers
      num[i] = number;//assigns them to respective spots
    }
    return num;///returns array 
  }
  public static int[] delete(int[] list, int pos){
    int[] array1 = new int[10];//default array
    if(pos >= 0 & pos <= 9){//checks if position is within array alloacted space 0-9
      int length = list.length - 1;//only one number is removed so new arrays length will be one less the previous array
      int[] array2 = new int[length];///initialize and declare edited array
      for(int i = 0; i < 10; i++){
        if(i < pos){//list has one more space than array2 
          array2[i] = list[i];
        }
        if(i == pos){///skips intdened index
          continue;
        }
        if(i > pos){//after index is reached this modification makes so there will be no error of overfilling array
          array2[i - 1] = list[i];
        }
      }
      System.out.println("Index " + pos + " element is removed.");
      return array2; ///states what method accomplished and returns array
    }
    else{
      for(int i = 0; i < 10; i++){///since index given was outside arrays bounds
        array1[i] = list[i];///new array is the same as previous
      }
      System.out.println("The index is not valid.");//states error
    }
    return array1;
  }
  public static int[] remove(int[] list, int target){
    int targetCounter = 0;
    int[] array1 = new int[10];///makes default array
    for(int i = 0; i < list.length; i++){
      if(list[i] == target){//counts how many times target number appears
        targetCounter++;
      }
    }
    if(targetCounter == 0){//if it never shows up array will simply be copied
      for(int i = 0; i < 10; i++){
        array1[i] = list[i];
      }
      System.out.println("Element " + target + " was not found");///nothing was found
    }
    else{
      int length = list.length - targetCounter;//if target was found
      int[] array2 = new int[length];//adjusts new array length to fit all numbers expect target
      int j = 0;//j is used to represent new array spaces and since the contiue skips the postincrement
      for(int i = 0; i < list.length; i++){//i gets ahead and the loop finishes with new array filled correctly
        if(list[i] == target){//skips target number
          continue;
        }//loops and copys desired numbers in new array
        array2[j] = list[i];
        j++;
      }
      System.out.println("Element " + target + " was found");
      return array2;
    }
    return array1;
  }
  public static void main(String[]args){
 Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }

  }
