///Travis Barnes 
///CSE002 hw 8 Program 1
///CSE2Linear
///This code takes 15 inputs puts them into an array and allows user to search for specfic inputs 
import java.util.Random;
import java.util.Scanner;
public class CSE2Linear{
  public static int[] gradeCheck(int[] grades, int i){///checks potential inputs for array
    Scanner scan = new Scanner (System.in);   
    boolean check = false;
    int input = 0;
    System.out.print((i+1) + ": ");
    check = scan.hasNextInt();///checks to see if int
    if (check == true){
      input = scan.nextInt();///if true puts in place holder variable
      if(input >= 0 & input <= 100){
        grades[i] = input;///if within range places inside array
        if(i > 0 && grades[i] > grades[i - 1] ){
          return grades;///if value is larger than previous then keep
        }
        else if(i > 0 && grades[i] < grades[i - 1] ){
          System.out.println("Input was not larger than previous. Try again.");
          gradeCheck(grades,i);
          return grades;//if value is smaller than previous loops method for proper value
        }
      }
      else{
        System.out.println("Integer is out of range. Try again.");
        gradeCheck(grades,i);
        return grades;///if value is out of range then loops method
      }
    }     
    else{
      String junk = scan.next();///throws away improper input
      System.out.println("Input is not an integer. Try again.");
      gradeCheck(grades,i);///loops method for proper value
      return grades;
    }  
    return grades;
  }
  public static void listArray(int[] grades){////Used as a short hand to print array
    for(int i = 0; i < 15; i++){
      System.out.print(grades[i] + " ");
    }
  }
  public static int numberCheck(){
    Scanner scan = new Scanner(System.in);
    boolean check = scan.hasNextInt();
    int number = 0;
    if(check == true){
      number = scan.nextInt();
      return number;
    }
    else if(check == false){
      String junk = scan.next();
      System.out.print("Wrong input, Try again.");
      number = numberCheck();
      return number;
    }
    return number;
  }
  public static void scramble(int[] grades){///Mixes the order of values in the array
    Random randomNumber = new Random();
    for(int i = 0; i < 15; i ++){
      int num = randomNumber.nextInt(15);
      int temp = grades[i];
      grades[i] = grades[num];
      grades[num] = temp;
    }
  }
  public static void linearSearch(int[] grades, int target){
    for(int i = 0; i < 15; i++){//linear search combs through the array in order
      if(grades[i] == target){
        System.out.println(target + " was found in " + (i+1) + " iterations.");
        return;// stops at the loop when found and ends method
      }
    }
    System.out.println(target + " was not found in " + 15 + " iterations.");
  }
  public static void binearySearch(int[] grades, int target){
    int check = (14/2);///bineary search starts in the middle of the array
    int search = 4;///counter that will be cut in half every iteration
    if(target <= grades[14] & target >= grades[0]){
    for(int i = 1; i < 5; i++){///longet search will only be 4 iterations 
      if(grades[check] < target){//if target is bigger than number found 
        check += search ;//moves to upper half
        search /= 2;
        continue;
      }
      else if(grades[check] > target){//if target is lower than number found
        check -= search;//moves to lower half
        search /=2;
        continue;
      }
      else if(grades[check] == target){//ends search and prints number of iterations 
        System.out.println(target + " was found in " + i + " iterations");
        return;
      }
    }
    }  
    System.out.println(target + " was not found in 4 iterations");///if not in array gives this message
  }
  public static void main(String[]args){
    Scanner scan = new Scanner (System.in);   
    int[] grades = new int[15];//initializes array
    System.out.println("Enter 15 ascending ints for final grades in CSE2 ");
    for(int i = 0; i < 15; i ++){//loops through array
      gradeCheck(grades, i);//calls method that'll ask for numbers and check order   
    }
    
    ///displays grades
    listArray(grades);
    System.out.println();
    ///bineary search 
    System.out.print("Enter a grade to search for: ");
    int find = numberCheck();
    binearySearch(grades,find);
    ///scramble then reprint
    scramble(grades);
    listArray(grades);
    System.out.println();
    ///linear search 
    System.out.print("Enter a grade to search for: ");
    int search = numberCheck();
    linearSearch(grades,search);
  }
}