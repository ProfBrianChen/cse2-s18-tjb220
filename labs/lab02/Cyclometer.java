/////Travis Barnes 
///// 2/2/2018
/////CSE02 section 122
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
///Setting counts and time variables for trip 1 and 2
int secsTrip1=480;  //
int secsTrip2=3220;  //
int countsTrip1=1561;  //
int countsTrip2=9037; //
//Setting variables for calculations
double wheelDiameter=27.0,
  	PI=3.14159,
  	feetPerMile=5280,  
  	inchesPerFoot=12,   
  	secondsPerMinute=60; 
///Setting variables for trip distances       
double distanceTrip1, distanceTrip2,totalDistance;
      
      /////Prints number of minutes for Trip 1     
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	    /////Prints number of minutes for Trip 2
      System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");

///Calculating Trip 1 distance in inches
	distanceTrip1=countsTrip1*wheelDiameter*PI;
///Calculating Trip 1 distance in Miles      
	distanceTrip1/=inchesPerFoot*feetPerMile; 
///Calculating Trip 2 in Miles      
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
///Summation of both trips      
  totalDistance=distanceTrip1+distanceTrip2;
      
      
///Prints distance of Trip 1
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
///Prints distance of Trip 2      
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
///Prints combined distance of both trips      
	System.out.println("The total distance was "+totalDistance+" miles");
	}  //end of main method   
} //end of class





