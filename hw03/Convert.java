/////Travis Barnes
////CSE02 Section 122
////hw03 Convert
////This program will take acre and rain fall info and convert it to cubic miles of rain

import java.util.Scanner;///import Scanner
public class Convert {
    ///Main method for every java program
    public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in); 
      
    System.out.print("Enter the affected area in acres: ");
    double areaAcres = myScanner.nextDouble();///recieves area in acres
    
    System.out.print("Enter inches of rainfall in the affected area: ");
    double inchesRain = myScanner.nextDouble();///records inches of rainfall      
  
////Calculation variables////
    double gallonsRain;///gallons of rain from acres and inches of rain
    double cubicMiles;///cubic miles of rain   
    double gallonToInch3 = 231;///conversion from gallons to cubic inches  
    double inchesFeet3 = Math.pow(12,3);///moving from cubic inches to cubic feet
    double feetMiles3 = Math.pow(5280,3);//moving from cubic feet to cubic miles
    
    gallonsRain = areaAcres * inchesRain * 27154;////Looked up a conversion from acres and inches to gallons
    cubicMiles = ((gallonsRain * gallonToInch3) / inchesFeet3) / feetMiles3; ///converts gallons to cubic miles
    
    System.out.println(cubicMiles + " cubic miles of rain");  
    ///Prints total cubic miles of rain  
      
      
      
      
    }
}