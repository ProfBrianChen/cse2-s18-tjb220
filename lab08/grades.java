//////
import java.util.Scanner;
import java.util.Random;
public class grades{
public static void main(String[]args){
  Scanner scan = new Scanner (System.in);
  Random randomGenerator = new Random();
  int number = randomGenerator.nextInt(5) + 5;
  String[] students = new String[number];
  int[] midterm = new int[number];
  System.out.println("Enter " + number + " students names: ");
  for(int i = 0; i < number; i++){
    students[i] = scan.next();
  }
  for(int j = 0; j < number; j++){
    int grades = randomGenerator.nextInt(101);
    midterm[j] = grades;
  }
  System.out.println("Here are the midterm grades of the " + number + " students");
  for(int k = 0; k < number; k++){
  System.out.println(students[k] +  ": " + midterm[k]);
  }
}
}