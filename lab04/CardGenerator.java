////Travis Barnes 
////Lab04
////
////



public class CardGenerator {
  public static void main(String[] args){
    int card1 = (int)(Math.random()* 52)+1;
    String suit;
    String name;
    int cardNum = card1 % 13;

 
    switch(cardNum){
      case 1:
        System.out.print("You've picked Ace of ");
        break;  
      case 11:
        System.out.print("You've picked Jack of ");
        break;
      case 12:
        System.out.print("You've picked Queen of ");
        break;
      case 0:
        System.out.print("You've picked King of ");
        break;
      default:
        System.out.print("You've picked " + cardNum + " of ");
        break;  
    }   
   
   
    if(card1 < 13){
        suit = "Diamonds";
    }
    else if(card1 > 12 && card1 < 26){
        suit = "Clubs";
    } 
    else if(card1 > 25 && card1 < 39){
        suit = "Hearts";
    }
    else{
        suit = "Spades";            
    }    
  
    System.out.println(suit);
  }
}