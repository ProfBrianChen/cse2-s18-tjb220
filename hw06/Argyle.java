////Travis Barnes
////CSE002 section 122
////Hw06
////Creates argyle patterns
////This program breaks the argyle pattern into four different sections
////the program uses modulus operators to alternate between sections to fit accordingly
import java.util.Scanner;
public class Argyle{

public static void main (String[]args){
Scanner scan = new Scanner(System.in);
////initialize inputs
int m = 0;
int n = 0; 
int argyleWidth = 0;
int diamondWidth = 0;
//truefalse input variables   
boolean height = false;
boolean width = false;
boolean widthA = false; 
boolean widthD = false;  
/////asking for inputs    
while(height == false){////testing input value
System.out.print("Enter any positive integer height: ");
height = scan.hasNextInt();  
  if(height == true){
    m = scan.nextInt();
    if(m < 0){
      height = false;
      m = 0;
      System.out.print("Wrong Number, ");
    }    
  }  
  else{
    String junk = scan.next();
    System.out.print("Wrong input, ");
  }
}    
while(width == false){
System.out.print("Enter a positive integer width between 0-100: ");
width = scan.hasNextInt();  
  if(width == true){
    n = scan.nextInt();
    if(n < 0 | n > 100){
      width = false;
      n = 0;
      System.out.print("Wrong Number, ");
    } 
  }  
  else{
    String junk = scan.next();
    System.out.print("Wrong input, ");
  }
}
while(widthD == false){
System.out.print("Enter a positive integer Diamond width: ");
widthD = scan.hasNextInt();  
  if(widthD == true){
    diamondWidth = scan.nextInt();
    if(diamondWidth < 0){
      widthD = false;
      diamondWidth = 0;
      System.out.print("Wrong Number, ");
    }    
  }  
  else{
    String junk = scan.next();
    System.out.print("Wrong input, ");
  }
}     
while(widthA == false){
System.out.print("Enter an odd positive integer Argyle width(no larger than half of diamond width): ");
widthA = scan.hasNextInt();  
  if(widthA == true){
    argyleWidth = scan.nextInt();
    if(argyleWidth == 0){
      continue;
    }
    if(argyleWidth < 0 | argyleWidth % 2 == 0 | argyleWidth > diamondWidth / 2){//odd and smaller than half diamond
      widthA = false;
      argyleWidth = 0;
      System.out.print("Wrong Number, ");
    } 
  }  
  else{
    String junk = scan.next();
    System.out.print("Wrong input, ");
  }
}    
System.out.print("Give me first pattern character: "); 
String temp = scan.next();///no need to do anything since char 
char diamond1 = temp.charAt(0);  
System.out.print("Give me second pattern character: "); 
String temp2 = scan.next();
char diamond2 = temp2.charAt(0);  
System.out.print("Give me stripe character: "); 
String temp3 = scan.next();
char stripe = temp3.charAt(0);  

System.out.println("value check: " + m + " " + n + " " + diamondWidth + " " + argyleWidth + " " + diamond1 + " " + diamond2 + " " + stripe);  
////initalize values used in several independent scopes
int pattern = 1;
int b = diamondWidth;///remember to change to 0
int a = 0;
int pattern2 = 1;
int q = 0;  
int k = diamondWidth;
int s = argyleWidth / 2;  
for(int i = 0; i < m; ++i){////column length
  pattern = 1;///prints left to right so everyrow must start with a left most patterns
  for(int j = 0; j < n; ++j){////row length
    int p = j % diamondWidth;//as j increments across the row p will repeatedly go from 0 - diamondWidth
    q = i % diamondWidth;// same as p but as i goes down the columns
    if(pattern2 == 2){
    switch(pattern){
      case(2):///bottom right pattern
        b = diamondWidth;
        a = p + q;
        if(p == diamondWidth - 1){//once this finishes switches to the next pattern
          pattern = 1;
        }
        if(s >= p & p > s - argyleWidth & argyleWidth > 0){///prints stripe according to argyle length 
          System.out.print(stripe);
          continue;
        }     
        else if(b > a){//coninue is used so that as long a this condition is met none of the second symbol will print
          System.out.print(diamond2);
          continue;
        }
        System.out.print(diamond1);
        break;
      case(1):///bottom left pattern
        a = p - q;
        b = 0;
        if(p == diamondWidth - 1){
          pattern = 2;
        }
        if(p >= diamondWidth - 1 - s & p < (diamondWidth - 1) + argyleWidth - s  & argyleWidth > 0){
          System.out.print(stripe);
          continue;
        } 
        if(b <= a){
          System.out.print(diamond2);
          ++b;
          continue;
        }
        System.out.print(diamond1);
        break;
    }
    }
    else if(pattern2 == 1){
    switch(pattern){//both sets of switch statements are generally the same with a few things switched around 
      case(1):///top left pattern
        b = diamondWidth;
        a = p + q;
        if(p == diamondWidth - 1){
          pattern = 2;
        }
        if(s >= p & p > s - argyleWidth & argyleWidth > 0){
          System.out.print(stripe);
          continue;
        }    
        else if(b > a){
          System.out.print(diamond1);
          continue;
        }
        System.out.print(diamond2);
        break;
      case(2):///top right pattern
        a = p - q;
        b = 0;        
        if(p == diamondWidth - 1){
          pattern = 1;
        }
        if(p >= diamondWidth - 1 - s & p < (diamondWidth - 1) + argyleWidth - s  & argyleWidth > 0){
          System.out.print(stripe);
          continue;
        }
        else if(b <= a){
          System.out.print(diamond1);
          ++b;
          continue;
        }
        System.out.print(diamond2);
        break;
    } 
    }  
  }
  System.out.print("\n");
  ++s;

  if(q == diamondWidth - 1){///when length of diamond is reached 
    switch(pattern2){//when top layer is used switched to bottom etc
        case(1):
          pattern2 = 2;
          s = argyleWidth / 2;
          break; 
        case(2):
          pattern2 = 1;
          s = argyleWidth / 2;
          break;
    }
  }    
      
  }//end of i for loop
}    
}  