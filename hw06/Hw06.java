////Travis Barnes
////CSE002 section 122
////Hw06
///programs asks for user information and will loop when inputs are inncorrect
import java.util.Scanner;
public class Hw06{
  
public static void main(String[]args){
  Scanner scan = new Scanner (System.in);  
  ///initializing values 
  int courseNum = 0;
  int meetings = 0;
  int time = 0;
  int students = 0;
  String departmentName = " ";
  String instructorName = " ";
  ///initializing loop conditions 
  boolean timeT = false;
  boolean courseT = false;
  boolean department = false;
  boolean meetingsT = false;
  boolean instruct = false;
  boolean studentsT = false;
  while  (courseT == false){////Course Number
    System.out.print("What is your course number: ");////asks for number
    courseT = scan.hasNextInt();///checks if int
    if(courseT == true){  
      courseNum = scan.nextInt();///if true then keeps it
    }  
    else{
      System.out.println("Wrong input");///if false prints this and dumps input
      String junkWord = scan.next();      
    }
  }
  while  (department == false){////Department name  
    System.out.print("What is the department name: ");
    boolean departmentInt = scan.hasNextInt();
    boolean departmentDouble = scan.hasNextDouble();
    if(departmentInt == true | departmentDouble == true){  
      System.out.println("Wrong input");
      String junkWord = scan.next();
    }  
    else{
      department = scan.hasNext();
      departmentName = scan.next();   
    }
  }
  while  (timeT == false){////time for class
    System.out.print("When does your class start: ");
    timeT = scan.hasNextInt();
    if(timeT == true){  
      time = scan.nextInt();
    }  
    else{
      System.out.println("Wrong input");
      String junkWord = scan.next();      
    }
  }  
  while  (meetingsT == false){////meetings per week
    System.out.print("How many times a week does your class meet: ");
    meetingsT = scan.hasNextInt();
    if(meetingsT == true){  
      meetings = scan.nextInt();
    }  
    else{
      System.out.println("Wrong input");
      String junkWord = scan.next();      
    }
  }
  while  (instruct == false){////Instructor's name  
    System.out.print("What's your instructors name: ");
    boolean instructInt = scan.hasNextInt();
    boolean instructDouble = scan.hasNextDouble();
    if(instructInt == true | instructDouble == true){  
      System.out.println("Wrong input");
      String junkWord = scan.next();
    }  
    else{
      instruct = scan.hasNext();
      instructorName = scan.next();   
    }
  }
  while  (studentsT == false){////number of students
    System.out.print("How many students are in your class: ");
    studentsT = scan.hasNextInt();
    if(studentsT == true){  
      students = scan.nextInt();
    }  
    else{
      System.out.println("Wrong input");
      String junkWord = scan.next();      
    }
  }
  //////final print of collected data
  System.out.println("Course name: " + departmentName + " " + courseNum + "\n" +
                     "Starting time: " + time + "\n" +
                     "Meetings per week: " + meetings + "\n" +
                     "Instructor: " + instructorName + "\n" +
                     "Number of students: " + students);
  }
}