///Travis Barnes
///CSE002
///lab10
///multidimensional arrays
///
import java.util.Random;
public class RowColumn{
  public static void main(String[]args){
  Random randomNumber = new Random();
    int width = randomNumber.nextInt(5) + 1;///number of columns
    int height = randomNumber.nextInt(5) + 1;///number of rows
    int widthC = randomNumber.nextInt(5) + 1;
    int heightC = randomNumber.nextInt(5) + 1;
    boolean formatA = true;
    boolean formatB = false;
    boolean formatC = true;
    boolean formatD = true;
    System.out.println("Generating a matrix with width " + width + " and height " + height +".");
    int[][] A = increasingMatrix(width,height,formatA);
    printMatrix(A, formatA);
    System.out.println("Generating a matrix with width " + width + " and height " + height +".");    
    int[][] B = increasingMatrix(width,height,formatB);
    printMatrix(B, formatB);
    System.out.println("Generating a matrix with width " + widthC + " and height " + heightC +".");    
    int[][] C = increasingMatrix(widthC,heightC,formatC);
    printMatrix(C, formatC);    
    
    
    int [][] D = addMatrix(A,formatA, B, formatB);
    if(D != null){
    printMatrix(D, formatD);
    }
    D = addMatrix(A,formatA, C, formatC);
    if(D != null){
    printMatrix(D, formatD);
    }
  }
  public static int[][] increasingMatrix(int width, int height, boolean format){
    int numFill = 0;
    if(format == true){//row major
      int[][] array = new int[height][width];
      for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
          array[i][j] = numFill;
          numFill++;
        }
      }
      return array;
    }
    else{///column major
      int[][] array = new int[width][height];
      for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
          array[j][i] = numFill;//for column majors same process of for loop just flipped filling
          numFill++;
        }
      }
      return array;
    }
  }
  public static void printMatrix(int[][] matrix, boolean format){
    if(format == true){
      for(int i = 0; i < matrix.length; i++){
        System.out.print("[");
        for(int j = 0; j < matrix[0].length; j++){
          System.out.print(" " + matrix[i][j] + " ");
        }
        System.out.println("]");
      }
    }
    else{
      for(int i = 0; i < matrix[0].length; i++){////height for column
        System.out.print("[");
        for(int j = 0; j < matrix.length; j++){///width for column
          System.out.print(" " + matrix[j][i] + " ");
        }
        System.out.println("]");
      }
    }
  }
  public static int[][] translate(int[][] matrix ){
      int[][] copyArray = new int[matrix[0].length][matrix.length];
      for(int i = 0; i < matrix[0].length; i++){
        for(int j = 0; j < matrix.length; j ++){
          copyArray[i][j] = matrix[j][i];
        }
      }
      return copyArray;
  }
  public static int[][] addMatrix(int[][] a, boolean formatA, int[][] b, boolean formatB){
    System.out.println("Adding two Matrices.");
    printMatrix(a, formatA);
    System.out.println("plus");
    printMatrix(b, formatB);
    int aRows = 0;
    int aColumns = 0;
    int bRows = 0;
    int bColumns = 0;
    if(formatA == true){///finding dimensions of arrays
      aRows = a.length;
      aColumns = a[0].length;
    }
    else{
      aRows = a[0].length;
      aColumns = a.length;
    }
    if(formatB == true){
      bRows = b.length;
      bColumns = b[0].length;
    }
    else{
      bRows = b[0].length;
      bColumns = b.length;
    }
    
    if(aRows != bRows){///if dimensions are different can't add
      System.out.println("Unable to add input matrices.");
      return null;
    }
    else if(aColumns != bColumns){
      System.out.println("Unable to add input matrices.");
      return null;
    }
    
    if(formatB == false){
      System.out.println("translating column major to row major.");
      translate(b);
    }
    else if(formatA == false){
      System.out.println("translating column major to row major.");
      translate(a);
    }
    System.out.println("output:");
    int[][] newMatrix = new int[aRows][aColumns];
    for(int i = 0; i < aRows; i++){
      for(int j = 0; j < aColumns; j++){
        newMatrix[i][j] = a[i][j] + b[j][i];
      }
    }
    return newMatrix;
  }
}