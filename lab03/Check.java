/////Travis Barnes
////CSE02 Section 122
////Lab03 Check
////This program will calculate the total bill and will split it for the user 

import java.util.Scanner;///importing Scanner
public class Check {
    ///Main method for every java program
    public static void main(String[] args){
 
    Scanner myScanner = new Scanner (System.in);
    System.out.print("Enter the original cost of the check in the form xx.xx:  ");
    double checkCost = myScanner.nextDouble();///recieves original bill 
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number \n(in the form xx): ");
    double tipPercent = myScanner.nextDouble(); ////recieves tip amount
    tipPercent /= 100; //We want to convert the percentage into a decimal value
      
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();///Recieves number of people  
    
    ////Calculation variables////
    double totalCost;
    double costPerPerson;
    int dollars;////whole dollar amounts
    int dimes,pennies;//for storing digits 
      //right of the decimal point for cost
      
    totalCost = checkCost * (1 + tipPercent);///total plus tax
    costPerPerson = totalCost / numPeople;///double containing cost per person
    
    dollars = (int)costPerPerson; ///getting the dollar amount 
    dimes=(int)(costPerPerson * 10) % 10;///getting dime amount 
    pennies=(int)(costPerPerson * 100) % 10;///getting penny amount
    System.out.println("Each person in the group owes $ " + dollars + '.' + dimes + pennies);
    ////Prints total amount for each person
      
    }///End of main method
}////End of class