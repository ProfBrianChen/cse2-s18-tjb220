///Travis Barnes
///CSE002 homework 7 program 1
///Area
///This code asks the user for a shape and finds the area with given parameters
import java.util.Scanner;
public class Area{
  public static  double Triangle(double base,double height){//Method calculates area of triangle
   double Area = (base * height / 2);
   return Area;
 }
  public static double Circle(double diameter){//Method calcualtes area of circle
    double Area = (diameter/2)*(diameter/2)*3.14;
    return Area;
  }
  public static double Rectangle(double length,double width){//Method calculates area of rectangle 
    double Area = (length * width);
    return Area;
  }

  public static double doubleCheck(boolean input){//Checks if input is an double
    Scanner scan = new Scanner (System.in);
    double number = 0;
    input = scan.hasNextDouble();
    if(input == true){
      number = scan.nextDouble();
      return number;
    }
    else{
      String junk = scan.next();
      System.out.print("Wrong input, try again: ");
      input = false;
      number = doubleCheck(input);
      return number;
    }
  }
  public static String check(String input){//Checks shape input
    if(input.equals("triangle")){
      return "triangle";
    }
    else if(input.equals("rectangle")){
      return "rectangle";
    }
    else if(input.equals("circle")){
      return "circle";
    }
    else{
      return "Not applicable shape"; 
    }
  }
  
  public static void main(String [] args){///main methods asks for shape and dimensions 
    Scanner scan = new Scanner (System.in);
    String Shape = "";
    boolean answer = false;
    System.out.print("Find the area of a shape, ");
    while(answer == false){//loops question is input is inncorrect
      System.out.print("choose from a circle/triangle/rectangle: ");
      String input = scan.next();
      Shape = check(input);
      if(Shape.equals("Not applicable shape")){
        System.out.print("Wrong shape, ");
      }
      else{
        answer = true;
      }
    }
    
    boolean checkNumber = false;
    double Area = 0;
    switch(Shape){///follows shape path and uses methods to find area
      case("triangle"):
        System.out.print("Enter a base: ");
        double base = doubleCheck(checkNumber);
        System.out.print("Enter a height: ");
        double height = doubleCheck(checkNumber);
        Area = Triangle(base,height);
        break;  
      case("circle"):
        System.out.print("Enter a diameter: ");
        double diameter = doubleCheck(checkNumber);
        Area = Circle(diameter);
        break;
      case("rectangle"):
        System.out.print("Enter a length: ");
        double length = doubleCheck(checkNumber);
        System.out.print("Enter a width: ");
        double width = doubleCheck(checkNumber);
        Area = Rectangle(length, width);
        break;        
    }
    System.out.println("The Area is: " + Area);
  }
  
}