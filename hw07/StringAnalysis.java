///Travis Barnes
///CSE002 homework 7 program 2
///String Analysis
///Checks if all characters in a string are letters
import java.util.Scanner;
public class StringAnalysis{
  public static boolean stringCheck(String word){
    int length = word.length();
    int i = 0;
    for(; i < length; ++i){
      char letter = word.charAt(i);
      if(Character.isLetter(letter) == false){
        return false;
      }
    }
    return true;
  }
  public static boolean stringCheck(String word,int count){
    int length = word.length();
    if(length < count){
      count = length;
    }
    for(int i = 0; i < count; ++i){
      char letter = word.charAt(i);
      if(Character.isLetter(letter) == false){
        return false;
      }
    }
    return true;
  }
  public static int intCheck(boolean input){//Checks if input is an integer 
    Scanner scan = new Scanner (System.in);
    int number = 0;
    input = scan.hasNextInt();
    if(input == true){
      number = scan.nextInt();
      return number;
    }
    else{
      String junk = scan.next();
      System.out.print("Wrong input, try again: ");
      input = false;
      number = intCheck(input);
      return number;
    }
  }
  public static void main(String[] args){
    Scanner scan = new Scanner (System.in);    
    boolean check = false;
    boolean analyze = false;
    boolean result = false;
    System.out.print("Enter a String to be analyzed: ");
    String word = scan.next();
    while(analyze == false){
      System.out.print("Enter (1) to analyze whole string or (2) to analyzed certain amount of characters: ");
      int analyzeString = intCheck(check);
      if(analyzeString == 1){
        analyze = true;
        result = stringCheck(word);
      }
      else if(analyzeString == 2){
        analyze = true;
        System.out.print("How many characters are we checking? ");
        int count = intCheck(check);
        result = stringCheck(word,count);
      }
      else{
        System.out.println("Wrong choice try agian.");
      }
    }
    System.out.println("Were all the analyzed characters letters?  " + result);
  }
}