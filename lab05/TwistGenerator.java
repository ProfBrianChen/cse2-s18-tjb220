//////Travis Barnes
//////Lab 05
//////Twist Generator 
//////Takes an input length and makes a pattern 
import java.util.Scanner;
public class TwistGenerator{
    
public static void main (String []args){
Scanner scan = new Scanner (System.in);
///Setting up variables
int length = 0;
int counter1 = 0;
int counter2 = 0;
int counter3 = 0;  
boolean integer = false;  
///Overall while loop asking for a number will repeat if not positive or integer  
while(integer == false){
  System.out.print("Give a length (must be a positive integer): ");
  integer = scan.hasNextInt();///checks if integer
  if(integer == true){////records number
    length = scan.nextInt();   
    if(length >= 0){ 
      while(counter1 < length){////first line of pattern
        switch(counter1 % 3){
            case(0):
              System.out.print("\\");
              break;
            case(1):
                System.out.print(" ");
                break;
              case(2):
                System.out.print("/");
                break;
          }
          ++counter1;
      }
      System.out.print("\n");
      while(counter2 < length){////second line of pattern
        switch(counter2 % 3){
            case(0):
              System.out.print(" ");
              break;
            case(1):
              System.out.print("X");
              break;
            case(2):
              System.out.print(" ");
              break;
        }
        ++counter2;
      }
      System.out.print("\n");
      while(counter3 < length){////third line of pattern
        switch(counter3 % 3){
            case(0):
              System.out.print("/");
              break;
            case(1):
              System.out.print(" ");
              break;
            case(2):
              System.out.print("\\");
              break;
        }
        ++counter3;
      }
      System.out.print("\n");
    }
    else{
      integer = false;
      System.out.print("That was a Negative number, ");
    }
  }
  else{////Trashes number if not integer
  String junkWord = scan.next();
  System.out.print("Wrong input, ");  
  }  
}//closes while loop
}
}