///////Travis Barnes
///////HW02 due 2/6/2018
///////CSE002 Section 122

public class Arithmetic {
  
    public static void main(String[] args){

    
    int numPants = 3;//Number of pants
    int numBelts = 1;//Number of belts 
    int numShirts = 2;//Number of sweatshirts   
    double pantsPrice = 34.98;//Cost per pair of pants   
    double shirtPrice = 24.99;//Cost per shirt    
    double beltPrice = 33.99; //Cost per belt
    double paSalesTax = 0.06;//the tax rate
    
    ///////Calulations/////////     
    double costOfP = numPants * pantsPrice;//Calculating total cost of Pants 
    double pTax = costOfP * paSalesTax; ///Pants tax
    System.out.println("Total cost of pants: " +
                        (int)(costOfP * 100)/100.0 + "\nSales tax on pants: " + 
                        (int)(pTax * 100)/100.0  + "\n");//////Prints out receipt for pants  
   
    double costOfS = numShirts * shirtPrice;///Total cost of sweatshirts
    double sTax = costOfS * paSalesTax; ///Sweatshirts tax
    System.out.println("Total cost of sweatshirts: " +
                        (int)(costOfS * 100)/100.0 + "\nSales tax on sweatshirts: " + 
                        (int)(sTax * 100)/100.0  + "\n");//////Prints out receipt for sweatshirts        
   
    double costOfB = numBelts * beltPrice;///Total cost of belts
    double bTax = costOfB * paSalesTax; ///Belts tax  
    System.out.println("Total cost of belts: " +
                        (int)(costOfB * 100)/100.0 + "\nSales tax on belts: " + 
                        (int)(bTax * 100)/100.0  + "\n");//////Prints out receipt for belts 
      
    double salesTax = bTax + pTax + sTax;///sum of taxes
    double totalBTax = costOfB + costOfP + costOfS;///Cost before taxes 
    double totalATax = totalBTax + salesTax;///Overall total
    //////Prints Final receipt
    System.out.println("Total cost: " +
                        (int)(totalBTax * 100)/100.0 + "\nTotal sales tax: " + 
                        (int)(salesTax * 100)/100.0 + "\nTotal due: " +
                        (int)(totalATax * 100)/100.0  + "\n");
    }  
}