///Travis Barnes 
///CSE002
///hw10 - RobotCity
///Robots invade a city
import java.util.Random;
public class RobotCity{
  public static void main (String [] args){
    int[][] cityArray = buildCity();//throws city array into build city
    display(cityArray);
    System.out.println("Aliens have invaded the city!!!!");
    invade(cityArray);//changes city array
    display(cityArray);
    for(int i = 0; i < 5; i++){//continuously updates array by moving aliens right(eastward)
    System.out.println("They're moving!");
    update(cityArray);
    display(cityArray); 
    }

  }
  public static int[][] buildCity(){
    Random randomNumber = new Random();
    int[][] city = new int[randomNumber.nextInt(6) + 10][randomNumber.nextInt(6) + 10];//east-west and north-south dimensions 
    for(int i = 0; i < city.length; i++){
      for(int j = 0; j < city[0].length; j++){
        city[i][j] = randomNumber.nextInt(900) + 100;///assigns a random number to each city block
      }
    }
    return city;
  }
  public static void display(int[][] city){
    for(int i = city.length - 1; i >= 0; i--){///prints bottom up
      for(int j = 0; j < city[0].length; j++){///prints normally
        if(city[i][j] < 0){
          System.out.printf("%5s", "...");//Robots appear as this, most jarring change 
          continue;
        }
        System.out.printf("%5d", city[i][j]);//spaced out numbers evenly
      }
      System.out.println();
    }
  }
  public static void invade(int[][] city){
    Random randomNumber = new Random();
    int k = randomNumber.nextInt(25) + 25;//picks random number i didnt know what range so i wung it
    for(int i = 0; i < k; i++){//checks normally
      int EW = randomNumber.nextInt(city.length);
      int NS = randomNumber.nextInt(city[0].length);
      if(city[EW][NS] < 0){//if spot is taken loops again and reduces I to keep loop on track
        i--;
        continue;
      }
      city[EW][NS] = city[EW][NS] * (-1);//sets robot
    }
  }
  public static void update(int[][] city){
    for(int i = 0; i < city.length; i++){
      for(int j = city[0].length - 1; j >= 0 ; j--){//has to move right to left since it would cause errors 
        if(j == (city[0].length - 1) & city[i][j] < 0){
          city[i][j] = city[i][j] * (-1);//if negative on eastern most spot makes spot positive and moves on 
          continue;
        }
        if(city[i][j] < 0){//if normal negative makes spot positive and makes spot to the left negative
          city[i][j+1] = city[i][j+1] * (-1);
          city[i][j] = city[i][j] * (-1);
        }
      }
    }
  }
}