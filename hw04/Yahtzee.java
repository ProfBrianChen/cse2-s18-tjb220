/////////Travis Barnes
////CSE02 Section 122
////hw04 YAHTZEE
////This program plays Yahtzee

import java.util.Scanner;
public class Yahtzee{
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in);
    ///initializing dice rolls
    int die1 = 0;
    int die2 = 0;
    int die3 = 0;
    int die4 = 0;
    int die5 = 0;
    int dieNum;    
    System.out.println("<<<YAHTZEE>>>");      
    System.out.print("Would you like to Roll your Dice (1 for yes/ 2 for no): ");
    int answer = myScanner.nextInt();    
    if(answer == 1){///choice to randomly roll dice
      die1 = (int)(Math.random()* 6)+1;
      die2 = (int)(Math.random()* 6)+1;
      die3 = (int)(Math.random()* 6)+1;
      die4 = (int)(Math.random()* 6)+1;
      die5 = (int)(Math.random()* 6)+1;
    } 
    else if(answer == 2){///choice to pick numbers
      System.out.print("Choose your five numbers (XXXXX): ");
      dieNum = (int) myScanner.nextInt();
      int numLength = dieNum / 10000;
      if(numLength < 1 || numLength > 9){///has to be five numbers 
        System.out.println("Number incorrect \nTry Again");
      }  
      else{
        die1 = (int) dieNum / 10000;
        die2 = (int) (dieNum / 1000) % 10;
        die3 = (int) (dieNum / 100) % 10;
        die4 = (int) (dieNum / 10) % 10;
        die5 = (int) dieNum % 10;
        if(die1 > 6 || die1 < 1  || die2 > 6 || die2 < 1 || die3 > 6 || die3 < 1 || die4 > 6 || die4 < 1 || die5 > 6 || die5 < 0 ){
          die1 = 0;
          die2 = 0;
          die3 = 0;
          die4 = 0;
          die5 = 0;
          System.out.println("Number incorrect \nTry Again");
        }
      }
    }    
    else{
     System.out.println("Try Again");
    }  
    //counting dice numbers  
    int ones = 0;
    int twos = 0;
    int threes = 0;
    int fours = 0;
    int fives = 0;
    int sixes = 0;
    switch(die1){///records die1 roll
      case 1:
        ones = ones + 1;
        break;  
      case 2:
        twos = twos + 1;
        break;
      case 3:
        threes = threes + 1;
        break;
      case 4:
        fours = fours + 1;
        break;
      case 5:
        fives = fives + 1;
        break;
      case 6:
        sixes = sixes + 1;
        break;
    }
    switch(die2){
      case 1:
        ones = ones + 1;
        break;  
      case 2:
        twos = twos + 1;
        break;
      case 3:
        threes = threes + 1;
        break;
      case 4:
        fours = fours + 1;
        break;
      case 5:
        fives = fives + 1;
        break;
      case 6:
        sixes = sixes + 1;
        break;
    }
    switch(die3){
      case 1:
        ones = ones + 1;
        break;  
      case 2:
        twos = twos + 1;
        break;
      case 3:
        threes = threes + 1;
        break;
      case 4:
        fours = fours + 1;
        break;
      case 5:
        fives = fives + 1;
        break;
      case 6:
        sixes = sixes + 1;
        break;
    }
    switch(die4){
      case 1:
        ones = ones + 1;
        break;  
      case 2:
        twos = twos + 1;
        break;
      case 3:
        threes = threes + 1;
        break;
      case 4:
        fours = fours + 1;
        break;
      case 5:
        fives = fives + 1;
        break;
      case 6:
        sixes = sixes + 1;
        break;
    }
    switch(die5){
      case 1:
        ones = ones + 1;
        break;  
      case 2:
        twos = twos + 1;
        break;
      case 3:
        threes = threes + 1;
        break;
      case 4:
        fours = fours + 1;
        break;
      case 5:
        fives = fives + 1;
        break;
      case 6:
        sixes = sixes + 1;
        break;
    }

    /////upper section, significant amount print statements were used to check if values were being calculated properly///
    //System.out.println("Upper section scores: " );
    int Aces = ones;
   // System.out.println("Aces: " + Aces);
    int twosUpper = twos * 2;
   // System.out.println("Twos: " + twosUpper);
    int threesUpper = threes * 3;
    //System.out.println("Threes: " + threesUpper);
    int foursUpper = fours * 4;
    //System.out.println("Fours: " + foursUpper);
    int fivesUpper = fives * 5;
    //System.out.println("Fives: " + fivesUpper);
    int sixesUpper = sixes * 6;
    //System.out.println("Sixes: " + sixesUpper);
    /////lower section/////////
    //System.out.println("Lower section scores: " )
    int cDouble = 0;
    int cTriple = 0;
    int Triple = 0;
    int fourOfKind = 0;
    int fullhouse = 0;
    switch(ones){
      case 2:
        cDouble = cDouble + 1;
        break;  
      case 3:
        cTriple = cTriple + 1;
        Triple = 3;
        break;
      case 4:
        fourOfKind = 4;
    }
    switch(twos){
      case 2:
        cDouble = cDouble + 1;
        break;  
      case 3:
        cTriple = cTriple + 1;
        Triple = 6;
        break;
      case 4:
        fourOfKind = 8;
    }
    switch(threes){
      case 2:
        cDouble = cDouble + 1;
        break;  
      case 3:
        cTriple = cTriple + 1;
        Triple = 9;
        break;
      case 4:
        fourOfKind = 12;
    }
    switch(fours){
      case 2:
        cDouble = cDouble + 1;
        break;  
      case 3:
        cTriple = cTriple + 1;
        Triple = 12;
        break;
      case 4:
        fourOfKind = 16;
    }
    switch(fives){
      case 2:
        cDouble = cDouble + 1;
        break;  
      case 3:
        cTriple = cTriple + 1;
        Triple = 15;
        break;
      case 4:
        fourOfKind = 20;
    }
    switch(sixes){
      case 2:
        cDouble = cDouble + 1;
        break;  
      case 3:
        cTriple = cTriple + 1;
        Triple = 18;
        break;
      case 4:
        fourOfKind = 24;
    }
    if(cDouble == 1 && cTriple == 1){
      fullhouse = 25;
      Triple = 0;
    }   
    //System.out.println("Three of a kind: " + Triple);
    //System.out.println("Four of a kind: " + fourOfKind);
    //System.out.println("Full House: " + fullhouse);
    int smStraight = 0;
    int lgStraight = 0;
    if((ones > 0 && twos > 0 && threes > 0 && fours > 0 && fives == 0) || (ones == 0 && twos > 0 && threes > 0 && fours > 0 && fives > 0 && sixes == 0) || (twos == 0 && threes > 0 && fours > 0 && fives > 0 && sixes > 0)){
      smStraight = 25;  
    }/////small straight
    else if((ones > 0 && twos > 0 && threes > 0 && fours > 0 && fives > 0) || (twos > 0 && threes > 0 && fours > 0 && fives > 0 && sixes > 0)){
      lgStraight = 30;
    }/////large straight
    else{
      smStraight = 0;
      lgStraight = 0;
    }///Neither Straight
    //System.out.println("Sm. Straight: " + smStraight + "\nLg. Straight : " + lgStraight);
    int yahtzee = 0;///Yahtzee scoring
    if(ones == 5 || twos == 5 || threes == 5 || fours == 4 || fives == 5 || sixes == 5){
      yahtzee = 50;
    }
    //System.out.println("Yahtzee: " + yahtzee); 
    int chance = die1 + die2 + die3 + die4 + die5;//////Chance scoring 
    //System.out.println("Chance: " + chance);
   
    ////Final Scores 
    if(die1 != 0 && die2 != 0){
       System.out.println("Your roll was " + die1 + " " + die2 + " " + die3 + " " + die4 + " " + die5);
      int upperTotal = Aces + twosUpper + threesUpper + foursUpper + fivesUpper + sixesUpper;
      System.out.println("Your Upper section score: " + upperTotal);
    
      int bonus = 0;////We don't roll multiple times and Upper scores don't get very high so confused as to when this would even occur////
      if(upperTotal >= 63){
      bonus = 35;
      }
      System.out.println("Your Upper score plus bonus: " + (upperTotal + bonus));
    
      int lowerTotal = smStraight + lgStraight + yahtzee + fourOfKind + fullhouse + Triple + chance;
      System.out.println("Your Lower section score: " + lowerTotal);
   
      int grandTotal = upperTotal + bonus + lowerTotal;
      System.out.println("Your grand total: " + grandTotal);
    }
  }  
  
}