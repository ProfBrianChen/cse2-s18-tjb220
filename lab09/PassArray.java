//////Travis Barnes
//////Lab 08
//////Passing Arrays 
import java.util.Random;
public class PassArray{
  public static int[] copy(int[] array){
    int [] arrayCopy = new int[array.length];
    for(int i = 0; i < array.length; i++){
      arrayCopy[i] = array[i];
    }
    return arrayCopy;
  }
  public static void inverter(int[] array){ 
    int temp = 0;
    int length = array.length;
    for(int i = 0; i < length/2; i++){
      temp = array[i];
      array[i] = array[length - i - 1];
      array[length - i - 1] = temp; 
    }
  }
  public static int[] inverter2(int [] array){
    int []arrayCopy = copy(array);
    int temp = 0;
    int length = arrayCopy.length;
    for(int i = 0; i < length/2; i++){
      temp = arrayCopy[i];
      arrayCopy[i] = arrayCopy[length - i - 1];
      arrayCopy[length - i - 1] = temp; 
    } 
    return arrayCopy;
  }
  public static void main(String[]args){
    Random randomNumber = new Random();
    ////Inital array and its two copys
    int [] array0 = new int[8];
    for(int i = 0; i < 8; i++){
      array0[i] = randomNumber.nextInt(10);
    }    
    int [] array1 = copy(array0);
    int [] array2 = copy(array0);
    
    
    
    ////inverting array0
    inverter(array0);
    ///Putting array1 through inverter2
    array1 = inverter2(array0);
    ////passing array2 into inverter2 and giving array 3 that value
    int [] array3 = inverter2(array2);
    
    ///Printing arrays
   /* for(int i = 0; i < 8; i++){
      System.out.print(array0[i]);
    }
    System.out.println();
    for(int i = 0; i < 8; i++){
      System.out.print(array1[i]);
    }
    System.out.println();
    for(int i = 0; i < 8; i++){
      System.out.print(array3[i]);
    }
    System.out.println();*/
    System.out.print(array0);
  }
}