///Travis Barnes
///CSE002
///hw09 - Draw Poker
///This program will give out 5 random cards to two players and will see who has a winning hand.
import java.util.Random;
public class DrawPoker{
  public static void main(String[] args){
    Random randomNumber = new Random();
    int[] player1 = new int[5];///Initializes player hands and deck
    int[] player2 = new int[5];  
    int[] deck = new int[52];
    for(int i = 0; i < 52; i++){///assigns vlaues to the deck
      deck[i] = i;
    }
    shuffle(deck);///shuffles deck
    drawCards(player1,player2,deck);///dispurses cards
    winner(player1,player2);///declares winner

  }
  public static void shuffle(int[] deck){
    Random randomNumber = new Random();
    for(int i = 0; i < deck.length; i++){
      int space = randomNumber.nextInt(52);///picks random array space
      int temp = deck[i];
      deck[i] = deck[space];
      deck[space] = temp;///switches values
    }
  }
  public static void drawCards(int[] player1, int[] player2, int[] deck){
    int j = 0;
    int k = 0;
    for(int i = 0; i < 10; i++){
      if(i % 2 == 1){///flips betweens hands and assigns cards
        player2[j] = deck[i];
        j++;
        continue;
      }
      player1[k] = deck[i];
      k++;
    }
  }
  public static int[] handReader(int[] player){
    int[] copy = new int[5];
    for(int i = 0; i < 5; i++){//takes player hand and assigns suits to each card
      int suitNum = (int)(player[i]/13);//divides by 13 to find suit
      String suit = "";
      switch(suitNum){
          case(0):
            suit = "diamonds";
            break;
          case(1):
            suit = "clubs";
            break;
          case(2):
            suit = "hearts";
            break;
          case(3):
            suit = "spades";
            break;
      }
      int valueNum = player[i] % 13;
      String value = "" + (player[i]%13 + 1);///assigns string value to number on card
      switch(valueNum){///checks for high cards and assigns those
          case(0):
            value = "Ace";
            break;
          case(10):
            value = "Jack";
            break;
          case(11):
            value = "Queen";
            break;
          case(12):
            value = "King";
            break;
      }
      System.out.print(value + " of " + suit + " |");///prints card name and number
    }
    System.out.println();
    for(int i = 0; i < 5; i++){//creates copy of array with modulus values 
      copy[i] = player[i] % 13;
    }
    return copy;
  }
  
  public static int pairs(int[] player){///finds number of pairs
    int pairs = 0;
    for(int i = 0; i < 5; i++)///checks every card in hand
    {
      int mult = 0;///counts pairs
      for(int j = 0; j < 5; j++)///loops through hand to check for pairs
      {
        if(i == j)///skips card that is being checked out
        {
          continue;
        }
        if(player[i] == player[j])///if matching card adds to counter
        {
          mult++;
        }
      }
      if(mult == 1)
      {
        pairs += 1;
      }   
    }
    return (int)(pairs/2);//couldnt figure out to skip pair once found 
  }///so mult value will be twice as large as it needs to be 
  public static boolean triple(int[] player){//finds triples
    boolean triple = false;//same as pairs but will print out true or false instead of number of pairs
    for(int i = 0; i < 5; i++){
      int mult = 0;
      for(int j = 0; j < 5; j++){
        if(i == j){
          continue;
        }
        if(player[i] == player[j]){
          mult++;
        }
      }
      if(mult == 2){///if three of the same number are found breaks loop and return value
        triple = true;
        break;
      }   
    }
    return triple;
  }
  public static boolean flush(int[] player){///checks if cards have same suit
    String[] suits = new String[5];//creates a string array to fit suit names
    boolean flush = false;
    for(int i = 0; i < 5; i++){
      int suitNum = (int)(player[i]/13);
      switch(suitNum){
          case(0):
            suits[i] = "diamonds";
            break;
          case(1):
            suits[i] = "clubs";
            break;
          case(2):
            suits[i] = "hearts";
            break;
          case(3):
            suits[i] = "spades";
            break;
      }
    }
    if(suits[0] == suits[1] & suits[1] == suits[2] & suits[2] == suits[3] & suits[3] == suits[4]){
      flush = true;//returns true if all suits are the same
      return flush;
    }
    return flush;
  }
  public static boolean fullHouse(boolean pair, boolean triple){
    boolean fullhouse = false;///if there is a single pair and a double then theres a fullhouse
    if(pair == true & triple == true){
      fullhouse = true;
    }
    return fullhouse;
  }
  public static int handStrength(boolean onePair, boolean twoPair, boolean triple, boolean flush, boolean fullhouse){
    int handStrength = 0;///checks boolean values of each hand and assigns a comparable strength to the hand
    if(onePair == true){
      handStrength = 1;
    }
    if(twoPair == true){
      handStrength = 2;
    } 
    if(triple == true){
      handStrength = 3;
    }
    if(flush == true){
      handStrength = 4;
    }
    if(fullhouse == true){
      handStrength = 5;
    }
    return handStrength;
  }
  public static void winner(int[] player1, int[] player2){
    System.out.println("Player 1 has:");
    int[] copy1 = handReader(player1);///prints player hand and creates a modulus value array for the cards 0-13

    System.out.println("Player 2 has:");///same as above
    int[] copy2 = handReader(player2);
    
    int pairsP1 = pairs(copy1);///checks for copys using modulus arrays
    int pairsP2 = pairs(copy2);

    boolean onePairP1 = false;
    boolean twoPairP1 = false;  
    
    boolean onePairP2 = false;
    boolean twoPairP2 = false;  
    if(pairsP1 == 1){//Checks and how many pairs if any 
      onePairP1 = true;
    }
    else if(pairsP1 == 2){
      twoPairP1 = true;
    }
    if(pairsP2 == 1){
      onePairP2 = true;
    }
    else if(pairsP2 == 2){
      twoPairP2 = true;
    }        
    boolean tripleP1 = triple(player1);//checks for all card conditions
    boolean tripleP2 = triple(player2);
    
    boolean flushP1 = flush(player1);
    boolean flushP2 = flush(player2);

    boolean fullhouseP1 = fullHouse(onePairP1, tripleP1);
    boolean fullhouseP2 = fullHouse(onePairP2, tripleP2);

    int handStrength1 = handStrength(onePairP1, twoPairP1, tripleP1, flushP1, fullhouseP1);//assings hand strength
    int handStrength2 = handStrength(onePairP2, twoPairP2, tripleP2, flushP2, fullhouseP2);
    if(handStrength1 > handStrength2){///picks winner
      System.out.println("Player 1 wins");
    }
    else if(handStrength2 > handStrength1){
      System.out.println("Player 2 wins");
    }
    else{
      System.out.println("High card");
    }
  }
}